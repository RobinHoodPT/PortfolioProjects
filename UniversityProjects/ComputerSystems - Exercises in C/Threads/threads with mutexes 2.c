#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>
#include <pthread.h>
#include <limits.h>

#define ARRAY_SIZE 1000
#define THREADS_NUM 5

pthread_mutex_t mutexVec[THREADS_NUM];
int data[ARRAY_SIZE];
int result[ARRAY_SIZE];

void *calculate(void * element){
    int i,threadNum = (int)element;
    
    int qntToSearch = (ARRAY_SIZE/THREADS_NUM); //This is done in parallel
    for(i = 0;i<qntToSearch;i++){
        int pos = i+threadNum*qntToSearch;
        result[pos] = data[pos] *2 +10;
    }


    pthread_mutex_lock(&mutexVec[threadNum]); //Checks if it's your order to print
    printf("Thread Num %d: \n\n",threadNum+1);
    for(i = 0; i<qntToSearch;i++){
        int pos = i+threadNum*qntToSearch;
        printf("%d\n",result[pos]);
    }

    if(threadNum<THREADS_NUM-1){ //This is needed, because the last thread doesn't need to unlock.
         pthread_mutex_unlock(&mutexVec[threadNum+1]); //Unlocks the next thread in order
    }

    pthread_exit((void *)NULL);
}

int main (void){ 
    int i;
    pthread_t thread_id[THREADS_NUM];
    int args[THREADS_NUM];

    srand(pthread_self());
    for(i = 0; i<ARRAY_SIZE;i++){
        data[i] = (rand() % 100)+1;
    }    

    for(i = 0; i<THREADS_NUM;i++){
        pthread_mutex_init(&mutexVec[i],NULL);
        if(i>0){ //Only Locks all threads beside the first one for print order.
             pthread_mutex_lock(&mutexVec[i]);
        }       
    }    

    for(i = 0; i< THREADS_NUM; i++){
        args[i] = i;
        pthread_create(&thread_id[i],NULL,calculate,(void*)args[i]);
    }
	for(i = 0; i<THREADS_NUM;i++){ //Waits for all the threads.
       pthread_join(thread_id[i],NULL);
    }

    for(i = 0; i<THREADS_NUM;i++){
        pthread_mutex_destroy(&mutexVec[i]);
    }
    
    return 0;    
}