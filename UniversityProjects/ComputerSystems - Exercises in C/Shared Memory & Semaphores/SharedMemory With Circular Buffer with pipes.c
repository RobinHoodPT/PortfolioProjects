#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <time.h>


#define SHM "/ex11"
#define EXCHANGED_VALUES 30
#define ARRAY_SIZE 10
#define LEITURA 0
#define ESCRITA 1
typedef struct{
    int values[ARRAY_SIZE];
}Circular;

int main (void){
    int fd,data_size = sizeof(Circular),i,pipeReader[2],pipeWriter[2];
    int writer=0,reader = 0;

    if(pipe(pipeReader) == -1){
            perror("Pipe failed");
            return 1;
    }
    if(pipe(pipeWriter) == -1){
            perror("Pipe failed");
            return 1;
    }
        
    pid_t p = fork();
    if(p==-1){
        perror("Fork Failed!");
        exit(-1);
    }
    if(p==0){
        fd = shm_open(SHM,O_RDWR,S_IRUSR|S_IWUSR);
        if(fd == -1){
            perror("SHM_OPEN Failed:!\n");
            exit(-1);
        }

        if(ftruncate(fd,data_size)==-1){
            perror("Unable to ftruncate!");
            exit(-1);
        }
        

        Circular* c = (Circular*)mmap(NULL,data_size,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0);
        if(c == MAP_FAILED){
            perror("Unable to map from shared memory!");
            exit(-1);
        }
        

        close(pipeWriter[ESCRITA]);
        close(pipeReader[LEITURA]);



        for(i = 0; i<EXCHANGED_VALUES;i++){
            read(pipeWriter[LEITURA],&writer,sizeof(int));
            while(writer==reader);
            int value = (*c).values[reader];
            printf("Value obtained: %d \n",value);
            
            if(reader == ARRAY_SIZE-1){
                reader = 0;
            } else {
                reader++;
            }
            if(write(pipeReader[ESCRITA],&reader,sizeof(int))==-1){ 
                perror("Write error");
                exit(1);
            }            
        }
        close(pipeReader[ESCRITA]);
        close(pipeWriter[LEITURA]);
            
        if(munmap(c,data_size) == -1){
            perror("Unable to unmap");
            exit(-1);
        }

        if(close(fd)==-1){
            perror("Unable to close");
            exit(-1);
        }
        exit(1);
    }
   
    fd = shm_open(SHM,O_CREAT|O_EXCL|O_RDWR,S_IRUSR|S_IWUSR);
    if(fd == -1){
        perror("SHM_OPEN Failed: File Exists!\n");
         exit(-1);
    }

    if(ftruncate(fd,data_size)==-1){
        perror("Unable to ftruncate!");
        exit(-1);
    }

    Circular* c = (Circular*)mmap(NULL,data_size,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0);
    if(c == MAP_FAILED){
        perror("Unable to map from shared memory!");
        exit(-1);
    }

    /*if(write(fd[ESCRITA],&array,10*105)==-1){ 
        perror("Write error");
        exit(1);
    }
    read(fd[z][LEITURA],&productInfo,sizeof(info));
    */

    close(pipeReader[ESCRITA]);
    close(pipeWriter[LEITURA]);

    for(i = 0; i<EXCHANGED_VALUES;i++){       
        (*c).values[writer] = i;
        
        if(writer == ARRAY_SIZE-1){
            writer = 0;
        } else {
            writer++;
        }
        if(write(pipeWriter[ESCRITA],&writer,sizeof(int))==-1){ 
            perror("Write error");
            exit(1);
        }
        read(pipeReader[LEITURA],&reader,sizeof(int));
        if(writer==9){
            while(reader==0);
        } else {
            while(writer==(reader-1));
        }
    }
    close(pipeWriter[ESCRITA]);
    close(pipeReader[LEITURA]);

    if(munmap(c,data_size) == -1){
        perror("Unable to unmap");
        exit(-1);
    }

    if(close(fd)==-1){
        perror("Unable to close");
        exit(-1);
    }

    if(shm_unlink(SHM) == -1){
        perror("Unlink failed");
        exit(-1);
    }

    return 0;
}