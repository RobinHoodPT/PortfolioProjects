#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <time.h>


#define SHM "/ex08"
#define STR_SIZE 50
#define NR_DISC 10
#define MAX_GRADE 200000
#define TRUE 1
#define FALSE 0
typedef struct{
    int numero;
    char nome[STR_SIZE];
    int disciplinas[NR_DISC];
    int sync;
}aluno;

int main (void){
    int fd,data_size = sizeof(aluno),i;

        
    pid_t p = fork();
    if(p==-1){
        perror("Fork Failed!");
        exit(-1);
    }
    if(p==0){
        fd = shm_open(SHM,O_RDWR,S_IRUSR|S_IWUSR);
        if(fd == -1){
            perror("SHM_OPEN Failed:!\n");
            exit(-1);
        }

        if(ftruncate(fd,data_size)==-1){
            perror("Unable to ftruncate!");
            exit(-1);
        }

        aluno* a = (aluno*)mmap(NULL,data_size,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0);
        if(a == MAP_FAILED){
            perror("Unable to map from shared memory!");
            exit(-1);
        }

        while((*a).sync!=TRUE); //While not synced, wait.
        int highest=0,lowest=MAX_GRADE,sum=0;

        for(i = 0; i<NR_DISC;i++){
            int currentVal = (*a).disciplinas[i];
            if(currentVal > highest){
                highest = currentVal;
            }
            if(currentVal < lowest){
                lowest = currentVal;
            }
            sum += currentVal;
        }

        printf("Student id: %d \nStudent name: %s \nHighest Grade: %d \nLowest Grade: %d \nAverage Grade: %d \n",
        (*a).numero,
        (*a).nome,
        highest,
        lowest,
        sum/NR_DISC    
        );
        
        if(munmap(a,data_size) == -1){
            perror("Unable to unmap");
            exit(-1);
        }

        if(close(fd)==-1){
            perror("Unable to close");
            exit(-1);
        }
        exit(1);
    }
   
    fd = shm_open(SHM,O_CREAT|O_EXCL|O_RDWR,S_IRUSR|S_IWUSR);
    if(fd == -1){
        perror("SHM_OPEN Failed: File Exists!\n");
         exit(-1);
    }

    if(ftruncate(fd,data_size)==-1){
        perror("Unable to ftruncate!");
        exit(-1);
    }

    aluno *a = (aluno*)mmap(NULL,data_size,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0);
    if(a == MAP_FAILED){
        perror("Unable to map from shared memory!");
        exit(-1);
    }
    
    printf("Insert Student Number: \n");
    scanf("%d%*c",&((*a).numero));
    printf("Insert Student Name: \n");
    fgets((*a).nome,STR_SIZE,stdin);

    if((*a).nome[strlen((*a).nome)-1] == '\n' || (*a).nome[strlen((*a).nome)-1] == '\t'){
        (*a).nome[strlen((*a).nome)-1] = '\0';
    }

    for(i = 0; i< NR_DISC;i++){
        printf("Insert grade of disciplina %d: \n",i);
        scanf("%d%*c",&((*a).disciplinas[i]));
    }
    (*a).sync = TRUE;

    wait(NULL);    

    if(munmap(a,data_size) == -1){
        perror("Unable to unmap");
        exit(-1);
    }

    if(close(fd)==-1){
        perror("Unable to close");
        exit(-1);
    }

    if(shm_unlink(SHM) == -1){
        perror("Unlink failed");
        exit(-1);
    }

    return 0;
}