#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>

#define LEITURA 0
#define ESCRITA 1
#define STDIN 0
#define STDOUT 1
void factorial(void){
	int numIn;
	int numOut;
	int aux;
	
	read(STDIN,&numIn,sizeof(int));
	
	aux=numIn-1;
	numOut=numIn;
	while(aux>0){
		numOut=numOut*aux;
		aux=aux-1;
	}
	write(STDOUT,&numOut,sizeof(int));
}



int main (void){ //The last command is the parent process, cause we need to wait for the output and other commands to be done.
    int fd[2],fd2[2];

    /* cria o pipe */
    if(pipe(fd) == -1){//filho lê, pai escreve
        perror("Pipe failed");
        return 1;
    }
	if(pipe(fd2) == -1){//filho escreve, pai lê
        perror("Pipe failed");
        return 1;
    }
    pid_t p = fork(); //creates the child process that will read sort fx.txt

	if(p==-1){
        perror("Fork Failed");
        exit(-1);
    }
    if(p==0){ 
		close(fd[ESCRITA]);
		close(fd2[LEITURA]);
		
		dup2(fd[LEITURA],0); //redirects the input to the reading directory of the pipe
		dup2(fd2[ESCRITA],1); //redirects the output to the write directly through the pipe
					
		close(fd[LEITURA]);
		close(fd2[ESCRITA]);
		
		factorial();

        exit(0);
    }    
	
	close(fd[LEITURA]);
    close(fd2[ESCRITA]);
	
    int num;
    printf("Introduza um numero: \n");
    scanf("%d",&num);
	
	write(fd[ESCRITA],&num,sizeof(int));
	
	int result;
    if(read(fd2[LEITURA],&result,sizeof(int))!= 0){//reads file
        printf("PAI: %d\n",result);
    }	
    close(fd[LEITURA]);
    return 0;
}






