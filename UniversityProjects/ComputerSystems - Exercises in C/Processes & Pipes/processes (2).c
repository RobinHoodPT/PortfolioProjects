#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>

int cria_filhos(int qnt){ //Generate n child processes, returns index of creation
    int i;
    pid_t pid;
    for(i = 0; i<qnt;i++){
        pid=fork();
        if(pid == -1){
            perror("fork failed");
            exit(-1);
        }
        if(pid == 0){
            return i+1;
        }
    }
    return 0;
}

#define ARRAY_SIZE 2000
int main (void)
{
    int x = cria_filhos(6);
    if(x>0){
        exit(x*2);//if children process, exit the process multiplicating the process index * 2.
    }
    int i = 0;
    for(i = 0; i<6;i++){ //wait for all processes
        wait(NULL);
    }

  return 0;
}