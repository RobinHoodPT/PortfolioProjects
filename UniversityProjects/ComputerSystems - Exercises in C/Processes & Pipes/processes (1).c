#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>

#define ARRAY_SIZE 2000
int main (void)
{
    int numbers[ARRAY_SIZE];
    int i;

        /* intializes RNG (srand():stdlib.h; time(): time.h) */
        srand ((unsigned) time (NULL));

    /* initialize array with random numbers (rand(): stdlib.h) */
    for (i = 0; i < ARRAY_SIZE; i++)
        numbers[i] = rand () % 10000;

    /* initialize n */
    int n = rand () % 10000;
    int array[10];

    for(i = 0; i<10;i++){
        pid_t p = fork();
        if(p==-1){
            perror("Fork Failed!");
            exit(-1);
        }
        if(p==0){
            int j = 0;
            for(j = 0; j<200;j++){ //Loop through 200 values. 
                int z = j+(i*200); //Get the real position in the array based on the iteration
                if(n==numbers[z]){ //Get the number on that position, if true exit with the relative index
                    exit(j);
                } 
            }
            exit(255);//In case they don't find the number
        } else {
            array[i] = p; //Save the pid of the child in the array for later obtaining the real position from relative index
        }
    }

    
    int result;
    for(i = 0; i<10;i++){ //loop through all 10 processes

        waitpid(array[i],&result,0); //Wait for process i to end
        int y = WEXITSTATUS(result); //Obtain the relative index
        if(y!=255){ //If found
             printf("%d\n",y+(i*200)); //print the real index in the array
        }
    }

  return 0;
}