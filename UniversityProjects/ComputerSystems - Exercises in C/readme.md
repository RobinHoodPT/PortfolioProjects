In this project you can find some exercises I did in C on various topics of multithreaded programming(or multi core programming). Some of the topics are:
- Processes
- Pipes
- Shared Memory
- Semaphores
- Threads
- Mutexes

The exercises where done in a linux environment. 