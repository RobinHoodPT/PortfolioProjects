This project was made for the LAPR4(Laboratory/Project 4) where with the whole class constituted of about 20 students we needed to create a Spreadsheet app available for the internet. 
In this subject we needed to:
- Understand and apply agile techniques and methods in project management;
- Analyze, design, test, produce technical documentation and implement software to solve requirements in an incremental and iterative way;
- Work with a large team, in the context of an ongoing software development project, applying and agile approach, and using tools and approaches that are appropriate for each moment and context.

The project used the framework called GWT(or Google Web Toolkit)(http://www.gwtproject.org/) which allows us to easily create a web application with java code, while the framework would automatically convert it to javascript. Of course there were limitations. 

As this project was made by the whole class we were needed to divide the various use cases. The use-cases that were implemented by myself are the following:

1. Conditional Formating of Cells
2. Filter Range of Cells
3. Full XML Import/Export

U can check the respective folders for more information about the use cases.

In the respective folder there is not all of the code that was written for the use cases due to it being in shared classes which were edited alot. So could only get snippets of it, maybe when I have time.

At the beginning of the project we setup a deployment directly to teamcity and deployment to an azure server.
As written above the project follows the agile techniques, but most closely the Scrum Technique. I was apointed as the Scrum master for this project.
I ended the project with the evaluation of a 16.5 in 20.