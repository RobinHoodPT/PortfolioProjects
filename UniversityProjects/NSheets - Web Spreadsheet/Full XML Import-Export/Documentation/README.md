**Robin Couwenberg** (1161272) - Sprint 3 - IPC04.3
===============================

# 1. General Notes

This use case is highly dependent on the two previous feature increments. While they are developed and working they are missing alot of features and were not developed consciously taking in account each other. As for example the content it exports is not possible to be imported.

# 2. Requirements

The previous options should now include all the persisting elements of the workbooks (i.e., all the contents that are normally persisted with the workbook, such as macros, formatting styles, comments, etc.). The import should now ask the user if the file contents should replace or append the existing workbook contents.


# 3. Analysis

For this feature increment, I need to:  

- Understand how the import xml was implemented

- Understand how the export of xml was implemented

- Understand where all the data of the worksheet, such as macros, format styles, etc... is saved and how it's created.

## 3.1 Understanding of export xml

The exporting of xml is only working for a workbook(but it's considering a spreadsheet for some reason).

The exporting makes use of a DocumentBuilder Factory to get a singleton instance of a documentbuilder. It then creates an root element and starts to add it's respective xml tags and it's content to the file.

**Issues in the current version**
There are some issues in the current version, as the export exports all the cells with no row data. 
It also exports the value and not the content of an xml.

These issues will be corrected in this sprint.


## 3.1 Understanding of import xml

The importing of a xml is only working for a workbook. 

The importing of an xml file is also done with a document builder, obtained by a factory. It obtains all the elements by name followed by it's sub-elements. After obtaining all the data it creates the workbook.

**Issues in the current version**
There are some issues in the current version, just as it's only implemented to import at max 3 rows. 

These issues will be corrected in this sprint.

## 3.4 Analysis Diagrams

The main idea for the "workflow" of this feature increment.

**Domain Model (for this feature increment)**

Since this use case is importin/exporting it does not change any current domain classes.

**System Sequence Diagrams**

![Analysis SD](analysis.png)

**System Sequence Diagrams**

![Analysis SD2](analysis2.jpg)

# 4. Design

*In this section you should present the design solution for the requirements of this sprint.*


## 4.1. Tests

In this case the use case is highly dependent on the UI so there's no possibility of realizing unit tests. 

## 4.2. Requirements Realization


![SD US1](design1.png)

![SD US2](design2.png)


## 4.3. Classes

Classes:
- Workbookview
- FilterPresenter
- FilterController

## 4.4. Design Patterns and Best Practices

By memory we apply/use:  
- Singleton   
- GRASP 

# 5. Implementation

**Code Organization**  

We followed the recommended organization for packages:  
- Code should be added (when possible) inside packages that identify the group, sprint, functional area and author;
- For instance, we used **lapr4.blue.s2.core.n1161272**

# 6. Work Log

![workLog](workLog.png)








