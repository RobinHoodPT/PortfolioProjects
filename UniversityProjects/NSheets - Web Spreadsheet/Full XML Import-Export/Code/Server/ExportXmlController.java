/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.server.lapr4.green.s1.ipc.n1150461.workbooks.application;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import pt.isep.nsheets.shared.core.IllegalValueTypeException;
import pt.isep.nsheets.shared.core.Workbook;
import pt.isep.nsheets.shared.core.formula.green.s2.n1150461.GlobalVariable;
import pt.isep.nsheets.shared.core.lapr4.blue.s1.n1151314.macro.Macro;
import pt.isep.nsheets.shared.services.CellDTO;
import pt.isep.nsheets.shared.services.SpreadsheetCellsDTO;
import pt.isep.nsheets.shared.services.SpreadsheetDTO;
import pt.isep.nsheets.shared.services.WorkbookDTO;

/**
 *
 * @author cristianomelo + robin couwenberg
 */
public class ExportXmlController {
    
    private Document doc;
    private Element rootElement;

    public ExportXmlController() {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            doc = docBuilder.newDocument();
          } catch (ParserConfigurationException ex) {
            
        }
    }   
    
    public void exportWorkbook(String tagWorkBook, String tagSpreadsheet, String tagRow,WorkbookDTO workbook) throws ParserConfigurationException, TransformerException{
        Element workbookTag = doc.createElement(tagWorkBook);
        if(doc.hasChildNodes()){
            doc.appendChild(rootElement);
            rootElement.appendChild(workbookTag);
        } else {
            doc.appendChild(workbookTag);
        }
        
        Element wbTitleTag = doc.createElement("title");
        wbTitleTag.appendChild(doc.createTextNode(workbook.getName()));
        workbookTag.appendChild(wbTitleTag);
         Element wbDescTag = doc.createElement("description");
        wbDescTag.appendChild(doc.createTextNode(workbook.getDescription()));
        workbookTag.appendChild(wbDescTag);
        rootElement = workbookTag;
        
        List<SpreadsheetDTO> spreadsheets = (List<SpreadsheetDTO>) workbook.getSpreadsheets();
        for(SpreadsheetDTO sp : spreadsheets){
            exportSpreadsheet(tagSpreadsheet, tagRow, sp.getCells());
        }
        
        List<Macro> macros = Workbook.fromDTO(workbook).getMacrosList();
        Element wbMacros = doc.createElement("macros");
        workbookTag.appendChild(wbMacros);
        for(Macro m : macros){
            Element macro = doc.createElement("macro");
            wbMacros.appendChild(macro);
            Element macroName = doc.createElement("name");
            macroName.appendChild(doc.createTextNode("macroName")); //Macro has no possibility to get name(Probably not implemented)
            macro.appendChild(macroName);
            Element macroContent = doc.createElement("content");
            macroContent.appendChild(doc.createTextNode(m.getContent())); //Macro has no possibility to get name(Probably not implemented)
            macro.appendChild(macroContent);
        }

        List<GlobalVariable> globalVars = Workbook.fromDTO(workbook).getAllGlobalVariables();
        Element wbGlobalVars = doc.createElement("globalVars");
        workbookTag.appendChild(wbGlobalVars);
        for(GlobalVariable var : globalVars){
            Element globalVarTag = doc.createElement("var");
            wbGlobalVars.appendChild(globalVarTag);
            Element nameVarTag = doc.createElement("varName");
            nameVarTag.appendChild(doc.createTextNode(var.getName()));
            globalVarTag.appendChild(nameVarTag);
            Element contentVarTag = doc.createElement("varContent");
            contentVarTag.appendChild(doc.createTextNode(var.obtainValue().toString()));
            globalVarTag.appendChild(contentVarTag);
        }
        
    }

    public void exportSpreadsheet(String tagSpreadsheet, String tagRow,SpreadsheetCellsDTO spreadsheet) throws ParserConfigurationException, TransformerConfigurationException, TransformerException {
       Collection<CellDTO> cellCollection = spreadsheet.getCellSet();
       int numberRows = spreadsheet.getNumberRows();
       
        System.out.println("Size: " + numberRows);

        List<CellDTO> spreadsheetCells = (List<CellDTO>) cellCollection;
        int numberCellsPerRow = spreadsheetCells.size()/numberRows;


        Element spreadsheetTag=doc.createElement(tagSpreadsheet);
        if(doc.hasChildNodes()){
            rootElement.appendChild(spreadsheetTag);
        } else {
            doc.appendChild(spreadsheetTag);
        }        

        for(int i = 0; i<numberRows;i++){
             Element row=doc.createElement(tagRow);
             spreadsheetTag.appendChild(row);
             for(int j = 0; j<numberCellsPerRow;j++){
                 Element cell=doc.createElement("cell");
                cell.appendChild(doc.createTextNode(spreadsheetCells.get(j+i*numberCellsPerRow).getValue()));
                row.appendChild(cell);
             }
        }
    }
    
    public void exportXML(){
          TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("NSHEETSExport.xml"));
            transformer.transform(source, result);
          
        } catch (TransformerConfigurationException ex) {
            System.out.println(ex.getMessage());
        } catch (TransformerException ex) {
            System.out.println(ex.getMessage());
        }            
    }

}
