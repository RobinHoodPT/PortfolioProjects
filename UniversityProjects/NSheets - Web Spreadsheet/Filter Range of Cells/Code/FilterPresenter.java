/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.client.application.workbook.lapr4.blue.s2.n1161272.Filter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.regexp.shared.RegExp;
import gwt.material.design.addins.client.window.MaterialWindow;
import gwt.material.design.client.data.component.RowComponent;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialColumn;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.MaterialToast;
import gwt.material.design.client.ui.table.MaterialDataTable;
import pt.isep.nsheets.client.application.workbook.WorkbookView;
import pt.isep.nsheets.client.application.workbook.WorkbookView.SheetCell;
import pt.isep.nsheets.shared.core.Cell;
import pt.isep.nsheets.shared.core.IllegalValueTypeException;
import pt.isep.nsheets.shared.core.formula.compiler.FormulaCompilationException;
import pt.isep.nsheets.shared.services.lapr4.blue.s2.core.n1161272.Filter.Application.FilterController;

/**
 * The presenter which will intereact with the ui to filter the spreadhseet tables
 * @author Robin Couwenberg - 1161272
 */
public class FilterPresenter {
    
    private MaterialWindow filterWindow;
    private MaterialTextBox startCellBox;
    private MaterialTextBox endCellBox;
    private MaterialTextBox selectedColumnBox;
    private MaterialTextBox expressionBox;
    private MaterialDataTable<WorkbookView.SheetCell> customTable;
    private MaterialDataTable<WorkbookView.SheetCell> originalTable;
    
    private static final int ASCII_ALPHABET_SIZE = 26;
    private static final int ASCII_A_NUM = 65;

    public FilterPresenter(MaterialWindow filterWindow, MaterialDataTable<WorkbookView.SheetCell> customTable) {
        this.filterWindow = filterWindow;
        this.customTable = customTable;
        originalTable = new MaterialDataTable<>(customTable.getView());
        expressionBox = (MaterialTextBox) filterWindow.getContent().getChildren().get(2);
        MaterialRow row = (MaterialRow) filterWindow.getContent().getChildren().get(3);
        MaterialColumn columnStartCell = (MaterialColumn) row.getChildren().get(0);
        startCellBox = (MaterialTextBox) columnStartCell.getChildren().get(1);
        MaterialColumn columnEndCell = (MaterialColumn) row.getChildren().get(1);
        endCellBox = (MaterialTextBox) columnEndCell.getChildren().get(1);
        selectedColumnBox = (MaterialTextBox) filterWindow.getContent().getChildren().get(5);
        MaterialButton filterButton = (MaterialButton) filterWindow.getContent().getChildren().get(6);
        filterButton.addClickHandler((event) -> {
            if(inputFilter()){
                closeWindow();
            }
        });
         MaterialButton deleteFilterButton = (MaterialButton) filterWindow.getContent().getChildren().get(7);
        deleteFilterButton.addClickHandler((event) -> {
            if(removeFilter()){
                closeWindow();
            }
        });
    }
    
    private boolean inputFilter(){
        FilterController cntrl = new FilterController();
        
        String expression = expressionBox.getText();
        if(!validateExpression(expression)){
            MaterialToast.fireToast("The first character needs to be a boolean operator.");
            return false;
        }
        
        String startCell = startCellBox.getText();
        if(!validateCell(startCell)){
            MaterialToast.fireToast("The Start cell is invalid!");
            return false;
        }
        String endCell = endCellBox.getText();
        if(!validateCell(endCell)){
            MaterialToast.fireToast("The Ending cell is invalid!");
            return false;
        }
        
        RegExp regexNum = RegExp.compile("[A-Za-z]+");
        int initialRowNum = Integer.valueOf(regexNum.replace(startCell, ""));
        int finalRowNum = Integer.valueOf(regexNum.replace(endCell, ""));
        
        if(initialRowNum>finalRowNum){
            MaterialToast.fireToast("The start cell needs to be a higher row than the ending cell.");
            return false;
        }
        
        String selectClmn = selectedColumnBox.getText();
         if(!selectClmn.matches("^[A-Za-z]+")){
            MaterialToast.fireToast("The Selected Column is invalid!");
            return false;
        }
        int columnNum = 0;
        for(int i = 0; i<selectClmn.length();i++){
            int asciiNum = (int)selectClmn.charAt(i);
            columnNum += (asciiNum - ASCII_A_NUM) + i*ASCII_ALPHABET_SIZE;
        }
        GWT.log("Intial Row String " + startCell);
        GWT.log("Intial Row Num " + initialRowNum);
        GWT.log("Final Row String " + endCell);
        GWT.log("Final Row Num " + finalRowNum);
        for(int i = initialRowNum-1;i<=finalRowNum-1;i++){
            String tmpExpression = expression;
            try {
                RowComponent<SheetCell> row = customTable.getRow(i);
                Cell c = row.getData().getCell(columnNum);
                tmpExpression = c.getContent() + tmpExpression; 
                GWT.log(tmpExpression);
                if(!tmpExpression.startsWith("=")){
                    tmpExpression = "=" + tmpExpression;
                }
                boolean valid = cntrl.validateFormula(c,tmpExpression);
                if(valid){
                    row.removeFromParent();
                }
            } catch (FormulaCompilationException ex) {
               MaterialToast.fireToast("The formula is not correct! " + ex.getMessage());
               return false;
            } catch (IllegalValueTypeException ex) {
               MaterialToast.fireToast("The formula is not a boolean formula!");
               return false;
            } catch (Exception ex){
                
            }
        }
        return true;
    }
    
    
    public void openWindow(){
        MaterialWindow.setOverlay(true);
        filterWindow.open();
    }
    
    public void closeWindow(){
        filterWindow.close();
    }

    private boolean validateExpression(String expression) {
        return !expression.matches("^[A-Za-z0-9 ].*");
    }
    
    private boolean validateCell(String cell){
        return cell.matches("^[A-Za-z]+[1-9][0-9]*$");
    }
    public boolean removeFilter(){
        customTable = originalTable;
        customTable.getView().setRedraw(true);
        customTable.getView().refresh();
        return true;
    }
    
}
