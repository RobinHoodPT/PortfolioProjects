/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.shared.services.lapr4.blue.s2.core.n1161272.Filter.Application;

import com.google.gwt.core.client.GWT;
import pt.isep.nsheets.shared.core.Cell;
import pt.isep.nsheets.shared.core.IllegalValueTypeException;
import pt.isep.nsheets.shared.core.Value;
import pt.isep.nsheets.shared.core.formula.Expression;
import pt.isep.nsheets.shared.core.formula.compiler.ExcelExpressionCompiler;
import pt.isep.nsheets.shared.core.formula.compiler.FormulaCompilationException;

/**
 * The controller which will validate the expressions
 * @author Robin
 */
public class FilterController {
    
    public boolean validateFormula(Cell cell,String expression) throws FormulaCompilationException, IllegalValueTypeException{
        ExcelExpressionCompiler comp = new ExcelExpressionCompiler();
        Expression exp = comp.compile(cell, expression);
        Value val = exp.evaluate();
        GWT.log(val.toBoolean() + "");
        return val.toBoolean();
    }
    
}
