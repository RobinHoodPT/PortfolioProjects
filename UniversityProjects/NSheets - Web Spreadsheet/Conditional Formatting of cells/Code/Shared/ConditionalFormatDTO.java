/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.shared.services.lapr4.blue.s1.lang.n1161272.StyleExtension.application;

import java.io.Serializable;
import pt.isep.nsheets.shared.core.Address;
import pt.isep.nsheets.shared.core.Cell;
import pt.isep.nsheets.shared.core.CellImpl;

/**
 * DTO Of Class ConditionalFormat
 * @author Robin Couwenberg - 1161272
 */

public class ConditionalFormatDTO implements Serializable{
    
    private Address cellAddress;
    
    private String expression;
    /**
     * Only for Serializable purposes
     */
    public ConditionalFormatDTO() {
        this.cellAddress = null;
        this.expression = "";
    }
    /**
     * Default constructor for Condtional Format DTO
     * @param cellAddress
     * @param expression - The expression to create the formula for
     */
    public ConditionalFormatDTO(Address cellAddress, String expression) {
        this.cellAddress =  cellAddress;
        this.expression = expression;
    }
    /**
     * Obtain the cellAddress of this Conditional Format DTO
     * @return cellAddress
     */
    public Address getCellAddress() {
        return cellAddress;
    }
    /**
     * Obtain the expression of this Conditional Format DTO to transform in a Formula
     * @return Expression String
     */
    public String getExpression() {
        return expression;
    }
  
}
