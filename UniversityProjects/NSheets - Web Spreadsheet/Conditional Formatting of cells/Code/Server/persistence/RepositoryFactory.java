/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.server.lapr4.blue.s1.lang.n1161272.StyleExtension.persistence;



/**
 * @author Paulo Gandra Sousa
 *
 */
public interface RepositoryFactory {
    
    ConditionalFormatRepository conditionalFormatRepository();
}
