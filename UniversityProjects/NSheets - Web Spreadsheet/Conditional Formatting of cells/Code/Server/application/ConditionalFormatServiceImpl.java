/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.nsheets.server.lapr4.blue.s1.lang.n1161272.StyleExtension.application;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import pt.isep.nsheets.server.lapr4.blue.s1.lang.n1161272.StyleExtension.domain.ConditionalFormat;
import pt.isep.nsheets.shared.core.IllegalValueTypeException;
import pt.isep.nsheets.shared.core.formula.compiler.FormulaCompilationException;
import pt.isep.nsheets.shared.services.lapr4.blue.s1.lang.n1161272.StyleExtension.application.ConditionalFormatDTO;
import pt.isep.nsheets.shared.services.lapr4.blue.s1.lang.n1161272.StyleExtension.application.ConditionalFormatService;

/**
 * Service Implementation for all controllers of ConditionalFormatting
 * @author Robin
 */
public class ConditionalFormatServiceImpl extends RemoteServiceServlet implements ConditionalFormatService {
    
    /**
     * New Conditional Format and obtain the result
     * @param dto - the DTO with the content needed for the conditional format
     * @return the result of the expression
     * @throws FormulaCompilationException Throws if there's an error with the formula
     * @throws IllegalValueTypeException  throws if the formula is not a boolean condition
     */
    @Override
    public boolean newConditionalFormatAndObtainResult(ConditionalFormatDTO dto) throws FormulaCompilationException, IllegalValueTypeException{
        //TODO Add check if alraedy in database or not.
        ConditionalFormat cf = new ConditionalFormat(dto.getCellAddress(), dto.getExpression());
        boolean result = cf.evaluateFormula();
        //Save to database
        return result;
    }
    
}
