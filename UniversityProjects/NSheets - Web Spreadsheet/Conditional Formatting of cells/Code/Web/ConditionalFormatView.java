package pt.isep.nsheets.client.application.workbook.lapr4.blue.s1.n1161272.ConditionalFormat;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import gwt.material.design.addins.client.window.MaterialWindow;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialColumn;
import gwt.material.design.client.ui.MaterialIcon;
import gwt.material.design.client.ui.MaterialLabel;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.MaterialToast;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.isep.nsheets.shared.core.Cell;
import pt.isep.nsheets.shared.core.IllegalValueTypeException;
import pt.isep.nsheets.shared.core.formula.compiler.FormulaCompilationException;
import pt.isep.nsheets.shared.services.lapr4.blue.s1.lang.n1161272.StyleExtension.application.ConditionalFormatDTO;
import pt.isep.nsheets.shared.services.lapr4.blue.s1.lang.n1161272.StyleExtension.application.ConditionalFormatService;
import pt.isep.nsheets.shared.services.lapr4.blue.s1.lang.n1161272.StyleExtension.application.ConditionalFormatServiceAsync;


public class ConditionalFormatView  {
    
    
        private MaterialWindow conditionalFormatWindow;
        private MaterialIcon buttonFormula;
        private MaterialTextBox textBox;


    public ConditionalFormatView(MaterialWindow conditionalFormatWindow, MaterialLabel targetCell, Cell cell) {
        this.conditionalFormatWindow = conditionalFormatWindow;
        //GWT.log(conditionalFormatWindow.getContent().getChildren().get(2).getClass().getName());
//        GWT.log("2 " + conditionalFormatWindow.getContent().getChildren().get(3).getClass().getName());
//        GWT.log("2 " + conditionalFormatWindow.getContent().getChildren().get(4).getClass().getName());
        textBox =  (MaterialTextBox) conditionalFormatWindow.getContent().getChildren().get(2);
        GWT.log(conditionalFormatWindow.getContent().getChildren().get(4).getClass().getName());
        MaterialRow row = (MaterialRow) conditionalFormatWindow.getContent().getChildren().get(4);
        GWT.log(row.getChildren().get(0).getClass().getName());
        MaterialColumn columnValid = (MaterialColumn) row.getChildren().get(0);
        GWT.log(columnValid.getChildren().get(1).getClass().getName());
        
        MaterialTextBox backgroundColor1 = (MaterialTextBox) columnValid.getChildren().get(1);
        
        MaterialColumn columnNotValid = (MaterialColumn) row.getChildren().get(1);
        GWT.log(columnNotValid.getChildren().get(1).getClass().getName());
        
        MaterialTextBox backgroundColor2 = (MaterialTextBox) columnNotValid.getChildren().get(1);
        
        buttonFormula =  (MaterialIcon) conditionalFormatWindow.getContent().getChildren().get(5);
        
        
        buttonFormula.addClickHandler(event -> {
            if(!textBox.getText().startsWith("=")){
                textBox.setText("="+textBox.getText());
            }
            
            if(backgroundColor1.getText().length() == 7 && backgroundColor2.getText().length() == 7 && backgroundColor1.getText().startsWith("#") && backgroundColor2.getText().startsWith("#")){
            //MaterialToast.fireToast(textBox.getText());
            String result = "";
            ConditionalFormatServiceAsync service = GWT.create(ConditionalFormatService.class);
            ConditionalFormatDTO dto = new ConditionalFormatDTO(cell.getAddress(), textBox.getText());
            AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
				public void onFailure(Throwable caught) {
                                    if(caught instanceof IllegalValueTypeException){
                                        Window.alert("Error: Not a Boolean Condition.");
                                    } else {
                                        Window.alert("Error: " + caught.getMessage());
                                    }
				                        }
				public void onSuccess(Boolean result) {
					MaterialToast.fireToast("Formula Accepted!", "rounded");
                                        if(result){
                                            targetCell.getElement().getStyle().setBackgroundColor(backgroundColor1.getText());
                                        } else {
                                            targetCell.getElement().getStyle().setBackgroundColor(backgroundColor2.getText());
                                        }
				}
			};
            service.newConditionalFormatAndObtainResult(dto, callback);
            } else {
                 Window.alert("Error: Please indicate a correct background color in hex code.");
            }
        });
    }

        public void openWindow(){
            MaterialWindow.setOverlay(true);
            conditionalFormatWindow.open();
        }
        
//        @UiHandler("obtainResultBtn")
//        void onClick(SelectionEvent<Element> event){
//          MaterialToast.fireToast("Selected: " + event.getSelectedItem().getInnerText());
//        }
}
