**Robin Couwenberg** (1161272) - Sprint 1 - Lang03.1
===============================

# 1. General Notes

As this User Story is an extension of the Core08.1 and Core02.1, the creation of this use case will be highly limited. There will be time spent to help both of these user stories as they influence the creation and results of this one. With the appointment as scrum master the organization and the checking will also be consuming some time on the creation of this use case.

# 2. Requirements

Lang03.1 
Update the "style" extension so that it can be used for the conditional formatting of cells based on the result of the execution of formulas. 
For the style of a cell to be conditional it must have an associated formula and two formatting styles. One of the styles is applied when the formula evaluates to true and the other when it evaluates to false. 
The editing of these settings should be done in a window.

List of possible style changes:
- Color (background)

# 3. Analysis

For this feature increment, since it is the first one to be developed in this sprint I need to:  

- Understand how the application works and also understand the key aspects of GWT, since it is the main technology behind the application  

- Understand how the Sheets and cells are implemented (for instance, how the UI gets the Cell style that are displayed)  

- Understand how to create it as an extension of the current program.

## 3.1 Analysis Diagrams

**Domain Model (for this feature increment)**

![Domain Model](domainmodel.png)

- **Domain Model**. Since this user story is an extensions we are limited to saving all the date necessary for the class itself, for it to work. In this case the ConditionalFormat class it the special entity uniquely created for this extension which will house the cell it was created for, the expression/formula for the condition and future the style to change too based on the formula(Not yet implemented, depends on the implementation of other user stories.

##3.2 Formulas

The formula to change the style the content will be a boolean formula, which the result may only TRUE or FALSE.

Depending on the result of the formula a row in the selected range will be hidden from view. 

- True: When the condition is true for the specific cell the row is changed to the respective style
- False: Whenever the condition is false for the specific cell in the colum is changed to the respective style

##3.3 Styles

The change of the cells is by obtaining the html element of the respective cell widget. Then changing the background color.

# 4. Design

## 4.1. Requirements Realization

*In this section you should present the design realization of the requirements.*

**For US1**

![SD US1](design1.png)

Notes:  
- The diagram only depicts the less technical details of the scenario;  
- For clarity reasons details such as the PersistenceContext or the RepositoryFactory are not depicted in this diagram.   
- **ConditionalFormatService** realizes the GWT RPC mechanism;  

## 4.2. Classes

*Present and describe the major classes of you solution.*

- **ConditionalFormat** is the major entity of the solution which will save and organise the requirements for this user story

## 4.4. Design Patterns and Best Practices

By memory we apply/use:
- Singleton
- Repository
- DTO

 
## 4.5. Tests

**Services/Controllers**

For the services we will be creating a ConditionaalFormatService.

Tests:
- Validate various formulas based on the content of the cells.

**Test:**  

	@Test
	public void testSimpleBooleanFormula() throws Exception {
		System.out.println("testSimpleBooleanFormula");
		String formulaSimple = "<5";
	}

Due to time limits the creation of tests was not done due to not being able to understand in time the functionality of the program.

# 5. Implementation

*If required you should present in this section more details about the implementation. For instance, configuration files, grammar files, etc. You may also explain the organization of you code. You may reference important commits.*
 

**Code Organization**  

We followed the recommended organization for packages:  
- Code should be added (when possible) inside packages that identify the group, sprint, functional area and author;
- For instance, we used **lapr4.blue.s1.lang.n1161272**

The code for this sprint:  
Project **server**    
ConditionalFormatServiceImpl
ConditionalFormat
ConditionalFormatRepository
Project **shared**  
ConditionalFormatDTO
ConditionalFormatService
CondtionalFormatServiceAsync

Project **NShests** 
Workbookview
workbookview.ui.xml
ConditionalFormatView

# 6. Final Remarks 

*In this section present your views regarding alternatives, extra work and future work on the issue.*

During the creation of the use case I got to some problems which limited the execution. The use case is not fully working, issues:

- Not really as an extensions. Due to the difficulty of the first sprint we decided that this use case would just be a feature instead of an extension due to non existence of the extension mechanism as of yet. Most code can be removed(extension style), but the UI not, as it was difficult for me to understand how it worked in such a short time frame.

- Not saving to database. While there are classes for persistence they are not being used and it's not implemented due to the sheer amount of time. It would not be hard to implement it.

- Styles are not associated with the conditional format, so need to add new value objects to the entity if we wanted to persist them. This was due to the time needed to understand the GWT UI and it's respective styles before I could start implementing it.

- Due to being unable to serialize a Cell, I'm unable to use current values in Cells as values in the condition. (Ex: A1<4 is not supported, only 5<7).

- As my objective was creating the use case by using the patterns, I was needed to take time to correctly understand their fundamentals. Some of them are implemented as explained above, but some are limited due to time constraints.
