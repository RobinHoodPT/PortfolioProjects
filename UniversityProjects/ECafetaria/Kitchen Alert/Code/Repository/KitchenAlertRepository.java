/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.booking.KitchenAlert;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Optional;

/**
 *
 * @author Robin
 */
public interface KitchenAlertRepository  extends DataRepository<KitchenAlert, Long>{
    
    Optional<KitchenAlert> findByID(Long id);
    
    public Iterable<KitchenAlert> getNotAcceptedKitchenAlerts();
    
}
