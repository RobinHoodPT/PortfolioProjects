/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.domain.booking.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Robin
 */
public class KitchenAlertBootstrapper implements Action {
    /**
     * Executes the kitchen alert bootstrapper
     * @return  True if was able to execute, false if not.
     */
    @Override
    public boolean execute() {
        
        return register();
    }

    private boolean register() {
        
        KitchenAlert a1 = new KitchenAlert(1);
        KitchenAlert a2 = new KitchenAlert(1);
        KitchenAlert a3 = new KitchenAlert(0);
        a1.accept();
        a2.accept();
        a3.accept();
        
        KitchenAlertRepository repo = PersistenceContext.repositories().kitchenAlert();
        
        try {
            repo.save(a1);
            repo.save(a2);
            repo.save(a3);
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(KitchenAlertBootstrapper.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
}
