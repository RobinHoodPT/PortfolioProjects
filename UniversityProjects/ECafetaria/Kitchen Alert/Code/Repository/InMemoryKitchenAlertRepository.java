/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.booking.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;
import java.util.Optional;

/**
 *
 * @author Robin
 */
public class InMemoryKitchenAlertRepository extends InMemoryRepository<KitchenAlert, Long>implements KitchenAlertRepository {

    @Override
    public Optional<KitchenAlert> findByID(Long id) {
        return matchOne(e -> e.id().equals(id));
    }

    @Override
    public Iterable<KitchenAlert> getNotAcceptedKitchenAlerts() {
        return match(e -> !e.isAccepted());
    }

    @Override
    protected Long newKeyFor(KitchenAlert entity) {
        return entity.id();
    }
    
}
