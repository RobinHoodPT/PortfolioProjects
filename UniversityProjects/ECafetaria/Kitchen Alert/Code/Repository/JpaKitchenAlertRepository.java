/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.booking.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import java.util.Optional;

/**
 *
 * @author Robin
 */
public class JpaKitchenAlertRepository extends CafeteriaJpaRepositoryBase<KitchenAlert, Long> implements KitchenAlertRepository {

    @Override
    public Optional<KitchenAlert> findByID(Long id) {
        return matchOne("e.id =:id", id);
    }

    @Override
    public Iterable<KitchenAlert> getNotAcceptedKitchenAlerts() {
        return match("e.accepted = FALSE");
    }
    
}
