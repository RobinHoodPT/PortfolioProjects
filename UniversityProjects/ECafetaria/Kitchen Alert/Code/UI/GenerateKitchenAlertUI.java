/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.Booking;

import eapli.ecafeteria.application.Booking.GenerateKitchenAlertController;
import eapli.ecafeteria.domain.booking.KitchenAlert;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;

/**
 *
 * @author Robin
 */
public class GenerateKitchenAlertUI extends SelectWidget<KitchenAlert>  {
    
    GenerateKitchenAlertController controller;

    public GenerateKitchenAlertUI(String header, Iterable<KitchenAlert> source) {
        super(header, source);
        controller = new GenerateKitchenAlertController();
    }    
    public boolean doShow(){
        System.out.println("------------------------------------------------------");
        this.show(); //Show the alerts List;
        KitchenAlert alert = selectedElement();
        int option = selectedOption();
        while(option != 0){
            System.out.println("------------------------------------------------------");
            if(Console.readBoolean("Accept Alert(Y/N)")){
                controller.acceptAlert(alert);
                source.remove(alert);
            }
            System.out.println();
            if(!source.isEmpty()){
                this.show(); //Show the alerts List;
                alert = selectedElement();
                option = selectedOption();
            } else {
                System.out.println("There are no more alerts to be shown.\n");
                option = 0;
            }            
        }        
        return true;
    }
    
    

    
    
    

    
}
