/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.app.backoffice.console.presentation.Booking;

import eapli.ecafeteria.application.Booking.GenerateKitchenAlertController;
import eapli.ecafeteria.domain.booking.KitchenAlert;
import eapli.framework.actions.Action;

/**
 *
 * @author Robin
 */
public class GenerateKitchenAlertAction implements Action{

    @Override
    public boolean execute() {
        GenerateKitchenAlertController ctrl = new GenerateKitchenAlertController();
        Iterable<KitchenAlert> source = ctrl.getNotAcceptedKitchenAlert();
        if(source.iterator().hasNext()){ //Checks if there are alerts
            GenerateKitchenAlertUI UI = new GenerateKitchenAlertUI("Kitchen Alerts: ", source);
            return UI.doShow();
        }
        return false;      
    }
    
}
