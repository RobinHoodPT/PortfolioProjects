/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.Booking;

import eapli.ecafeteria.domain.booking.KitchenAlert;
import eapli.ecafeteria.persistence.KitchenAlertRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Robin
 */
public class GenerateKitchenAlertController implements Observer{
    
    private final KitchenAlertRepository repo;
    /**
     * Default constructor for the controller. 
     */
    public GenerateKitchenAlertController() {
        repo = PersistenceContext.repositories().kitchenAlert();
    }   

    @Override
    public void update(Observable o, Object o1) {
        KitchenAlert alert = (KitchenAlert)o1; 
        try {
            repo.save(alert);
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(GenerateKitchenAlertController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Obtains all not accepted KitchenAlerts to be shown
     * @return List of not accepted kitchen Alert
     */
    public Iterable<KitchenAlert> getNotAcceptedKitchenAlert(){
        return repo.getNotAcceptedKitchenAlerts();
    }
    /**
     * Accept the kitchen alerts, not showing it anymore
     * @param ka the kitchen alert accepted
     * @return true if able to save the acceptance, false if not
     */
    public boolean acceptAlert(KitchenAlert ka){
        ka.accept();
        try {
            repo.save(ka);
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(GenerateKitchenAlertController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
}
