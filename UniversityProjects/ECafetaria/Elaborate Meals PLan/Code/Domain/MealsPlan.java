/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import eapli.ecafeteria.domain.mealmenu.MealMenu;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.domain.Designation;
import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.OneToMany;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Version;

/**
 *
 * @author Diogo Barreto(1150468) + Robin Couwenberg(1161272)
 */
@Entity
public class MealsPlan implements AggregateRoot<Long>, Serializable {

    private static final long serialVersionUID = 1L;
    private static Heuristic defaultHeuristics;

    // ORM unique key
    @Version
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long mealsPlanID;

    private final PlanStatus status;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<MealItem> mealItems;

    @OneToOne
    private final MealMenu menu;

    /**
     * MealsPlan Constructor. When creating the plan, it automatically set's the
     * status to open.
     *
     * @param menu - the menu referenced by the meals plan
     */
    public MealsPlan(MealMenu menu) {
        this.status = new PlanStatus();
        this.menu = menu;
        this.mealItems = new ArrayList<>();
    }
    /**
     * For ORM Purpose only
     */
    protected MealsPlan() {
         this.status = new PlanStatus();
         this.menu = null;
    }
    

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof MealsPlan)) {
            return false;
        }

        final MealsPlan that = (MealsPlan) other;
        if (this == that) {
            return true;
        }

        return id().equals(that.mealsPlanID) && status.equals(that.status);
    }

    /**
     *
     * @param mealsPlanID
     * @return
     */
    @Override
    public boolean is(Long mealsPlanID) {
        return id().equals(mealsPlanID);
    }

    @Override
    public Long id() {
        return this.mealsPlanID;
    }


    
    public boolean addMealItem(MealItem item){

        return mealItems.add(item);
    }

    @Override
    public String toString() {
        return menu.toStringPeriod();
    }

    public boolean isOpen() {
        return status.obtainStatus().equals(PlanStatusEnum.OPEN);
    }

     public void closeMealsPlan(){
        
         status.closePlan();
     }

    public Iterable<MealItem> getMealItems() {
        return mealItems;
    }
    public boolean isPastDeadline(){
        if(!mealItems.isEmpty()){
         for(MealItem mi: mealItems){
         Calendar deadline =mi.obtainMeal().date().obtainDate();
         Calendar currentDate= Calendar.getInstance();
         if(deadline.get(Calendar.YEAR) > currentDate.get(Calendar.YEAR)){
            currentDate.add(Calendar.DAY_OF_YEAR, -365);
         }
         if((deadline.get(Calendar.DAY_OF_YEAR) -currentDate.get(Calendar.DAY_OF_YEAR)) <2){
             return true;
         }
        }
        }
         return false;
    }
    public boolean isValid(){
        if(mealItems.isEmpty() || mealItems.size() < 1){
            return false;
        }
        return true;
    }
    
    public static void setDefaultHeuristic(Heuristic newheuristic) {
        defaultHeuristics = newheuristic;

    }

    /**
     * @return the defaultHeuristics
     */
    public static Heuristic getDefaultHeuristics() {
        return defaultHeuristics;
    }
}
