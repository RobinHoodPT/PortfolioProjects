/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.kitchen;

import eapli.framework.domain.ddd.ValueObject;
import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author Diogo Barreto 1150468 + Robin Couwenberg 1161272
 */
@Embeddable
public class PlanStatus implements  ValueObject, Serializable {
    
    private PlanStatusEnum status;
   //If status is 1, it means the plan is open for editing and if it's 0, then it's closed
    public PlanStatus(){
        this.status = PlanStatusEnum.OPEN;
    }
    
   public PlanStatusEnum obtainStatus(){
       return status;
   }
   
   public void closePlan(){
       this.status = PlanStatusEnum.CLOSED;
   }
    
    
    }
 
 
    
